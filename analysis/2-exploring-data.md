# Exploring Data

You can explore data to rapidly view trends and make comparisons by time and location using Explore:

1. To open the Report Manager, go to the Main Menu ( ![image alt text](analysis/image_45.png) )
2. Select Analysis > Explore
![image](analysis/image_46.png)   
<span class="coordinates">none</span>