# Report amendments

To approve Amendment Requests:

1. Go to the Tasks dashboard 
2. Click on the Amendment Request in the list
3. In the Amendment Request, review details of the Form, the Proposed Changes and the Reason for the Amendment.
![image](task-management/image_188.png)
<span class="coordinates">3,-10,244,west;</span>
4. Press the Approve or Reject ( ![image alt text](task-management/image_189.png) ) button to make your choice
5. The user will be sent an email to inform them of the outcome

![main-image](task-management/image_190.png)
<span class="coordinates">1,20,104,west;2,200,574,west;</span>

Notes:
- All amendment requests should normally be approved by a Location Administrator, who works at an Administrative level close to the staff responsible for reporting and can verify the reasons for making changes to reports.

Related guidance:
- [Types of user accounts](#/user-accounts/1-types-of-user-accounts) 
- [Editing reports after submission](#/reporting/5-editing-reports-after-submission) 
