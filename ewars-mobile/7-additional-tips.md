# Additional tips

1. Turn the phone off when not needed to save battery   
2. The phone should only be used for professional purposes.    
3. Do not leave it unattended or out in the open. Keep it locked in a drawer or cupboard when not it use.    
4. If lost of stolen, it must be reported immediately to your nearest EWARS Administrator.    
