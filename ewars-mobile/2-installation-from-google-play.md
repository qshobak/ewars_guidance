# Installation from Google Play

To install EWARS Mobile from Google Play:

1. Open the Google Play Store app from the desktop

![mobile-image](ewars-mobile/image_14.png)
<span class="coordinates">1,650,430,south;</span>
