# Editing reports

If you need to make an amendment to a report after it has been submitted, please inform your Divisional Surveillance Officer.
