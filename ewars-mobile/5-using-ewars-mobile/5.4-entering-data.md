# Entering data

You are now ready to enter data into the report. To do this:

1. Click on the data entry cells
2. Enter the values into the cells   
![mobile-image](ewars-mobile/image_77.png)
<span class="coordinates">1,595,531,west;2,921,687,west;</span>

Notes: 
* Form fields that are required are marked with an asterisk *
