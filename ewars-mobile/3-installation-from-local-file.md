# Installation from local file

If there is no reliable internet connection in the location where mobile devices will be setup to use EWARS Mobile, then it is possible to install EWARS Mobile from a local file.
Prior to travelling to the location where the devices will be installed:

1. Go to [http://cdn.ewars.ws/apk/app-release.apk](http://cdn.ewars.ws/apk/app-release.apk)
2. The EWARS Mobile APK file will automatically download and be saved to your and computer 