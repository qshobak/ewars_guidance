# Insert SIM card

1. If the deployment requires SMS based submission of data, insert a locally procured SIM card into the device.    
Or press **Next >** to skip the step    
![mobile-image](ewars-mobile/image7.png)
<span class="coordinates">1,1312,841,west;</span>

Related guidance:
- How to setup an SMS gateway
- How to manage mobile devices within an EWARS Account