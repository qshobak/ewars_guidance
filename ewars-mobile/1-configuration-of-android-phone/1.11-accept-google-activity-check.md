# Accept Google Activity Check

1. If prompted to allow Google to check device activity, press Accept   
![mobile-image](ewars-mobile/image12.png)
<span class="coordinates">1,896,841,west;</span>