# Navigating documents

To navigate through documents whilst they are open in the Browser:

1. Open the document in the browser
2. Use the Previous and Next ( ![image alt text](documents/image_68.png) ) buttons to scroll through the previous or next available document in the browser   

![main-image](documents/image_69.png)
<span class="coordinates">1,240,704,west;2,14,557,east;</span>
