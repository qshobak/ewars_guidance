# Viewing recently published documents

To view recently published reports for a specific Document:

1. Go to Recently Published Documents. This shows the latest documents published for each Document type
2. Click on a Document to open it in the browser

![main-image](documents/image_59.png)
<span class="coordinates">1,-19,280,south;2,51,600,west;</span>