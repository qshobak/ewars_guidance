# Conducting a case-investigation

If a case investigation is conducted as part of the risk assessment, for example to collect samples and confirm the aetiology of a specific biological hazard, then the rest of the investigation can be appended during the management of the alert.

1. Click on the Investigation tab at the bottom of the screen
2. Press the Create Form button to open the investigation forms that are associated with the type active alert
3. Select the form, complete the information and press submit   
![image](alert/image_34.png) 
<span class="coordinates">3,120,450,west;</span>
4. The result of the investigation will be appended to the alert and can be viewed by other users   
![image](alert/image_35.png)
<span class="coordinates">4,40,490,west;</span>

![main-image](alert/image_36.png)
<span class="coordinates">1,315,0,east;2,343,40,east;</span>


Notes:
- Investigations can only be appended to alerts that have reached the Risk Assessment stage. 
- Multiple investigations can be appended to the same alert if they are conducted. Similarly, different types of investigation forms can also be associated with the same alert type.
- Case investigations that are created and assigned to specific alerts can be amended and deleted from the Report Manager

Related guidance:
- [Defining form type](#/system-administration/6-form-manager/6.2-form-settings) 
