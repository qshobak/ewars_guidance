# Alert

<strong>A key role of surveillance is to trigger alerts to potential disease outbreaks at
the earliest possible interval. This is strongly linked with laboratory surveillance to confirm the cause of the outbreak. 

EWARS has a dedicated module to also support verification, risk assessment, and characterisation of a level of risk for each alert that is generated.
</strong>   

Table of contents<br><br>
[1. Defining alert thresholds](#/alert/1-defining-alert-thresholds)   <br>
[2. Notification of alerts](#/alert/2-alert-log)   <br>
[3. Management of alerts](#/alert/3-management-of-alerts)   <br>
[4. Re-opening an alert](#/alert/4-reopening-an-alert)    <br>
[5. Conducting a case investigation](#/alert/5-conducting-a-case-investication)   