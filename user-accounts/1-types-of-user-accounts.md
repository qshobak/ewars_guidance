# Types of users accounts

A summary of the key user types is provide below. A more brief description of the permissions available to each user is provided in the following sections.

<table>
  <tr>
    <td><strong>Role</strong></td>
    <td><strong>Example</strong></td>
  </tr>
  <tr>
    <td>Super Administrator</td>
    <td>Software developer, Project Technical Lead</td>
  </tr>
  <tr>
    <td>Global Administrator</td>
    <td>Project Technical Lead</td>
  </tr>
  <tr>
    <td>Account Administrator </td>
    <td>Senior WHO or MoH Epidemiologist at country level </td>
  </tr>
  <tr>
    <td>Location Administrator</td>
    <td>MoH District Health Officer, WHO Field Officer</td>
  </tr>
  <tr>
    <td>Reporting User</td>
    <td>MoH or NGO user at health facility level</td>
  </tr>
  <tr>
    <td>Organization Administrator</td>
    <td>Health Coordinator within Health NGO</td>
  </tr>
  <tr>
    <td>Laboratory Administrator</td>
    <td>Head of National Public Health Laboratory</td>
  </tr>
  <tr>
    <td>Laboratory User</td>
    <td>Laboratory Technician and country or field level</td>
  </tr>
</table>
