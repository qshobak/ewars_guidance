# Super Administrator
<table>
    <tr>
        <td valign="top"><strong>Example Title</strong></td>
        <td>Project Software Developer</td>
    </tr>
    <tr>
        <td valign="top"><strong>Type of profile</strong></td>
        <td>Software architect or developer, responsible for maintaining software code, testing features, fixing bugs.</td>
    </tr>
    <tr>
        <td valign="top"><strong>Description of role</strong></td>
        <td>Advanced, specialised role. Used for testing, orchestration and system-level diagnostics.</td>
    </tr>
</table>