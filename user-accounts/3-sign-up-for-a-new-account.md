# Sign up for a new user account

To create a new EWARS account:

1. Go to the EWARS website ([http://ewars.ws](http://ewars.ws))
2. Press the Sign Up button

![main-image](user-accounts/image_1.png)
<span class="coordinates">1, 9, 193, west; 2, 306, 641, west;</span>