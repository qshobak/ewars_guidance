# Sign in to an existing user account

If you have been invited to join EWARS, or already have an EWARS account, then to login you need to: 

1. Go to the EWARS website ([http://ewars.ws](http://ewars.ws))
2. Enter your email and password
3. Press Sign in

Related guidance:
- [What if you don't have an EWARS account?](#/user-accounts/3-sign-up-for-a-new-account)
- [What if you have forgotten your password?](#/user-accounts/4-resetting-user-password)

![main-image](user-accounts/image_0.png)
<span class="coordinates">1, 9, 193, west; 2, 138, 732, west; 3, 175, 655, west;</span>