# Selecting User Type

In the User Type form:

1. Select the type of user account you wish to request
2. Press the Next button

![main-image](user-accounts/image_3.png)   
<span class="coordinates">1, 140, 350, west;2, 293, 547, east;</span>

Related Guidance:
- [Types of user accounts](#/user-accounts/1-types-of-user-accounts)  