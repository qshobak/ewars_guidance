# Resetting user password

If you forget your password and need to reset it:

1. Go to the EWARS website ([http://ewars.ws](http://ewars.ws))
2. Click on Forgot Password?
3. In the Password Recovery form, Enter your email address and click on Request password.   
![image](user-accounts/image_7.png)
<span class="coordinates">none;<span/>
4. A new password will be sent to you by email.

![main-image](user-accounts/image_8.png)
<span class="coordinates">1, 9, 193, west; 2, 208, 511, east;</span>
