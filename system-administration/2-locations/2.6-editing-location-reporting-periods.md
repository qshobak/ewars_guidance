# Editing location reporting periods

To change the start or end date of reporting form in a specific location:

1. Click on a location to open it in the Locations Manager
2. In Reporting settings, you can view the form(s) and reporting periods available for the location
3. Click on the edit ( ![image alt text](system-administration/image_26.png) ) button to open the Reporting Period form
4. Edit the Start or End date 
![image](system-administration/image_27.png)
<span class="coordinates">4,120,300,east;</span>
5. Press the Save ( ![image alt text](system-administration/image_28.png) ) button to save your changes

![main-image](system-administration/image_29.png)
<span class="coordinates">1,110,150,west;2,140,251,east;3,140,711,east;5,25,253,west;</span>