# Creating new organizations

In the Account Manager:

1. Press the Add ( ![image alt text](system-administration/image_39.png) ) button
2. Click on + New Organization   
![image](system-administration/image_40.png)
<span class="coordinates">2,117,280,west;</span>

![main-image](system-administration/image_41.png)
<span class="coordinates">1,16,50,west;</span>