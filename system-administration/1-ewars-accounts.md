#EWARS Accounts

An EWARS Account usually represents a Country within the EWARS Application. 

When a new emergency occurs, or a new request for an EWARS deployment is made, the first steps is to create a new EWARS Account to manage the users, forms and data.

The ability to manage EWARS Accounts is only available to Global Administrators.

Notes:
- Do not confuse EWARS Accounts with User Accounts. User Accounts provide individual users with access to a specific EWARS Account

In the Account Manager:
1. Press the Add ( ![image alt text](system-administration/image_2.png) ) button    
2. Click on + New Account:   
![image](system-administration/image_3.png)
<span class="coordinates">2, 69, 240, west;</span>

![main-image](system-administration/image_4.png)
<span class="coordinates">1, 14, 52, west;</span>