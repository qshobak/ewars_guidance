# SMS Gateways

You can manage SMS Gateways using the SMS Gateway Manager:

1. To open the SMS Gateway Manager, go to the Main Menu ( ![image alt text](system-administration/image_123.png) )
2. Select Administration > SMS Gateways   
![image](system-administration/image_124.png)
<span class="coordinates">none;</span>
3. This will open the SMS Gateway Manager
