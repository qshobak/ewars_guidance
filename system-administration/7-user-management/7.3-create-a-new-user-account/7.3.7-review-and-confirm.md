# Review and Confirm

Finally, in the Review form:

1. Review the account details that you have entered
2. Press Create User add the User Account

![main-image](system-administration/image_95.png)
<span class="coordinates">1,168,527,west;2,308,590,east;</span>

Notes:
* The user will receive an email to notify them that the account has been generated
