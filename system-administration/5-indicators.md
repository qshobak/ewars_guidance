# Indicators

Indicators can be created and managed using the Indicator Manager:
1. To open the Indicator Manager, go to the Main Menu ( ![image alt text](system-administration/image_48.png) )
2. Select Administration > Indicators   
![image](system-administration/image_49.png)
<span class="coordinates">none;</span>
3. This will open the Indicator Manager  
