# Adding fields to the form

In the Form Template Manager:

1. Click the Fields Tab ( ![image alt text](system-administration/image_72.png) )
2. In **Form Editor**, you can build a data collection form by adding individual data fields
3. Press on + Add Field ( ![image alt text](system-administration/image_73.png) ) button to add new fields to the editor
4. Press Save Change(s) 

![main-image](system-administration/image_74.png)
<span class="coordinates">1,21,141,west;2,91,531,west;3,54,674,east;4,16,581,east;</span>