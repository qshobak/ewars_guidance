# Designing a template

In the Template Editor:

1. Click on the <\\> Template tab to open the HTML editor
2. Use the buttons at the top to enter content for the design
![image alt text](system-administration/image_145.png)
3. Press the Source ( ![image alt text](system-administration/image_146.png) ) button to toggle to view the HTML and the previewed content 

![main-image](system-administration/image_147.png)
<span class="coordinates">1,185,110,north;2,42,631,west;3,42,231,west;</span>

Notes:
* The report can also be fully designed in external HTML editors and pasted into the Source area of the EWARS Template Editor
* This is a specialised area that requires advanced skills using HTML and CSS. Support for the design and layout of country-specific templates can be obtained from support@ewars.ws
