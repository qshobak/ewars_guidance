# Editing reports after submission 

To edit a report after it has been submitted:

1. Open it in the Report Manager. The Report Viewer shows you the User who submitted the Report and the Date it was submitted
2. To edit the report, press Amend Report 
3. The data entry area is now editable, and you can make any required changes to the values.
4. Add a reason for the amendment into the description box.
![image](reporting/image_38.png)
<span class="coordinates">4,100, 200, west;</span>
5. After completing your edits, press the Submit Amendment ( ![image alt text](reporting/image_39.png) ) button   

Notes: 
- Amendments to report are not automatically accepted in EWARS and must be approved by an Administrator. 
- Once an amendment request has been made, further attempts to edit to the same report cannot be made until the pending request has been approved or rejected. 

Related guidance:
- [Managing report amendments](#/task-management/3-report-amendments) 

![main-image](reporting/image_40.png)
<span class="coordinates">1,-10, 531, east;2, -7,80,west; 3,310,530,west;</span>