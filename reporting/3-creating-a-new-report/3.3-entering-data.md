# Entering data 

In the Data Entry area of the report:

1. The fields will indicate the type of information that needs to be entered (number, text)
2. Some forms contain required cells must have values entered before they can be submitted. These will be highlighted in red if they are empty when you try to submit a form.   
![image](reporting/image_29.png)
<span class="coordinates">2,38,390,west;</span>

![main-image](reporting/image_30.png)
<span class="coordinates">1,330,550,west;</span>
