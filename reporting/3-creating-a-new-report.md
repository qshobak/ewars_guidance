# Creating a new report

To create a new report, you can either open it directly from the Surveillance Dashboard or by opening the Report Manager.

Related guidance:
- [Opening the Surveillance Dashboard](#/reporting/1-surveillance-dashboard) 
- [Opening the Report Manager](#/reporting/2-report-manager) 
